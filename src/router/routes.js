

const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Secciones.vue') },
      { path: 'secciones', component: () => import('pages/Secciones.vue') },
      {
        path: 'contenidos', component: () => import('pages/Contenidos.vue'),
      },
      { path: 'buscar', component: () => import('pages/Buscar.vue') },
      { path: 'cuenta', component: () => import('pages/Cuenta.vue') }
    ]
  },
  {
    path: '/login',
    component: () => import('pages/Inicio.vue')
  },
  { path: '/registro', component: () => import('pages/Registro.vue') }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
