import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex) // Habilita el uso del store desde 'cualquier lado'

export default function () {
  const store = new Vuex.Store({
    state: { // los datos que quiero 'compartir' en la aplicacion
      usuario: {
        id: Int32Array,
        nick: String,
        nombre: String,
        apellido: String,
        email: String,
        contrasenia: String,
        fechaNac: Date
      },
      logueo: false,

      obra: {
        poster: '',
        titulo: '',
        genero: '',
        anio: '',
        duracion: '',
        sinopsis: '',
        clasificacion: '',
        actores: '',
        ratingimdb: 0,
        tipo: '',
        id: ''
      },
      estadoModal: false
    },
    mutations: { // son funciones que modifican en estado
      logOn: function (state, user) {
        state.usuario.id = user.id
        state.usuario.nombre = user.nombre
        state.usuario.apellido = user.apellido
        state.usuario.email = user.email
        state.usuario.contrasenia = user.contrasenia
        state.usuario.nick = user.nick
        state.usuario.fechaNac = user.fechaNac
        state.logueo = true
      },
      logOff: function (state) {
        state.usuario.id = 0
        state.usuario.nombre = null
        state.usuario.apellido = null
        state.usuario.email = null
        state.usuario.contrasenia = null
        state.usuario.nick = null
        state.usuario.fechaNac = null
        state.logueo = false
      },

      changeValue: function (state, result) {

        state.obra.poster = result.poster
        state.obra.titulo = result.title
        state.obra.genero = result.genres
        console.log(result)


        state.obra.anio = result.year


        state.obra.duracion = result.runtime
        state.obra.sinopsis = result.plot
        state.obra.clasificacion = result.rated
        state.obra.actores = result.actors
        state.obra.ratingimdb = result.rating
        state.obra.tipo = result.type
        state.obra.id = result.imdbid

      },
      cambioModal: function (state) {
        state.estadoModal = true
      },
      modalOff: function (state) {
        state.estadoModal = false
      }
    }
  })

  return store
}
