const sqlite3 = require('sqlite3');

var db = new sqlite3.Database('recordviews.sqlite', (err) => {    
    if (err) {
        console.log('Error al abrir la base de datos:', err);
    } else {
        console.log('La base de abrio correctamente');
        crearTabla();
    }
});

const crearTabla = () => {
    console.log('Creando tabla...');
    db.run('CREATE TABLE IF NOT EXISTS persona (' +
           'id INTEGER PRIMARY KEY AUTOINCREMENT, ' + 
           'name TEXT, ' + 
           'age INTEGER)');
}

module.exports = db;
